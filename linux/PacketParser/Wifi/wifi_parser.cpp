#include "wifi_parser.h"
#include "../../util/my_log.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <pcap.h>
#include <errno.h>


void PrintMac(std::string strType, u_char* strMac)
{
    if(NULL == strMac)
        return;
        
    LOG_DEBUG("PrintMac[%s],[%s]",strType.c_str(),strMac);
    if( NULL == strMac )
    {
        std::cout<<"strMac is NULL."<<std::endl;
        return;
    }

    char macSource[MAC_BUFF_LEN];
    int offset = 0;
    for (int i=0; i<6; ++i)
    {

        sprintf(macSource + offset,"%02X:",strMac[i]);
        offset += 3;//字符按照2位大写输出，加上：需要位移3位
    }
    macSource [offset - 1] = '\0';//给字符串添加结尾符，顺便去除多余的：号
    //LOG_TRACE("Smac:[%s].",macSource);
    LOG_DATA("%s",macSource);
    //printf("%s:[%s].\n",strType.c_str(),macSource);
}

void CWifiParser::Parse(const struct pcap_pkthdr *header, const u_char *pkt_data)
{
    u_int uOffset = 0;
    if(RADIOTAP_HEAD_LEN > header->caplen)
    {
        std::cout<<"head length is too short.left["<<header->caplen<<"]"<<std::endl;
        return;
    }

    radiotap_hdr *radiotap = (radiotap_hdr *) (pkt_data);
    //根据系统字节序有不同的处理情况
    //u_int16_t nRadiotapLen = SWAP16(radiotap->it_len);
    u_int16_t nRadiotapLen = radiotap->it_len;
    uOffset += nRadiotapLen;
    if(FRAME_HEAD_TYPE_LEN > header->caplen - uOffset)
    {
        std::cout<<"head length is too short.radio["<<nRadiotapLen<<"]left["
            <<(header->caplen - uOffset)<<"]"<<std::endl;
        return;
    }
    //打印跟踪整个报文
    //int i;  
    //for(i=0; i<header->len; ++i)  
    //{  
    //  printf(" %02x", pkt_data[i]);  
    //  if( (i + 1) % 16 == 0 )  
    //  {  
    //      printf("\n");  
    //  }  
    //}

    //printf("\n\n");

    hdr_type *typeHdr = (hdr_type *) (pkt_data + uOffset);
    uOffset += FRAME_HEAD_TYPE_LEN;
    u_int8_t uSubType = typeHdr->type >>4;
    u_int8_t uType = (u_int8_t)(typeHdr->type <<4 )>>6;
    LOG_DEBUG("all:[%u]type[%u]subType[%u]uOffset[%u].\n",typeHdr->type,uType,uSubType,uOffset);
    if(0 != uType || (FRAME_HEAD_ADDRESS_LEN > header->caplen - uOffset))
    {
        LOG_TRACE("this is uType[%u]len[%d]\n",uType,header->caplen - uOffset);
        return;
    }
    hdr_address *addressHdr = (hdr_address *) (pkt_data + uOffset);

    switch(uSubType)
    {
    case 4:
        PrintMac("WifiUser",addressHdr->tranMac);
        break;
    case 5:
        PrintMac("WifiUser",addressHdr->desMac);
        PrintMac("WifiProvider",addressHdr->tranMac);
        break;
    default:
        return;
    }
}