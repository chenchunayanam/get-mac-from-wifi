#include <stdio.h>
#include <iostream>
#include <pcap.h>
//#include <direct.h>  
#include <unistd.h>
#include <string.h>
#include "util/util.h"
#include "util/my_log.h"
#include "PacketParser/Wifi/wifi_parser.h"

#define LINE_LEN 16
#define PCAP_ERRBUF_SIZE 256

/* packet handler 函数原型 */
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);

//初始化
MY_RESULT Init()
{
    return MY_SUCCESS;
}

int  GetPacketFormDev()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int inum;
    int i=0;
    pcap_t *adhandle;
    char errbuf[PCAP_ERRBUF_SIZE];/* 获取本机设备列表 */
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        return -1;
    }
    /* 打印列表 */
    for(d=alldevs; d; d=d->next)
    {
        printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else
            printf(" (No description available)\n");
    }
    if(i==0)
    {
        printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
        return -1;
    }
    printf("Enter the interface number (1-%d):",i);
    scanf("%d", &inum);
//    inum = 3;
    if(inum < 1 || inum > i)
    {
        printf("\nInterface number out of range.\n");/* 释放设备列表 */
        pcap_freealldevs(alldevs);
        return -1;
    }
    /* 跳转到选中的适配器 */
    for(d=alldevs, i=0; i< inum-1 ; d=d->next, i++)
        ;/* 打开设备 */

    /* open a device, wait until a packet arrives */   
    if ( (adhandle = pcap_open_live(d->name, 65535, 1, 0, errbuf)) == NULL)
    {
        fprintf(stderr,"\nUnable to open the adapter. %s is not supported by libpcap\n", d->name);
        pcap_freealldevs(alldevs);
        return -1;  
    }
    printf("\nlistening on %s(%s)...\n", d->name, d->description);
    /* 释放设备列表 */
    pcap_freealldevs(alldevs);

    /* wait loop forever */  
    int id = 0;  
    pcap_loop(adhandle, -1, packet_handler, (u_char*)&id);  
    pcap_close(adhandle); 

    return 0;
}/* 每次捕获到数据包时，libpcap都会自动调用这个回调函数 */

void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
//    //报文打印
//    printf("%d\n", header->caplen);  
//    for (bpf_u_int32 i = 0; i < header->caplen; ++i) 
//    {  
//      if (0 < i && 0 == i % 16) 
//          printf("\n");  
//      printf("%02x ", pkt_data[i]);  
//    }
    
    if (header->caplen < ETHER_HEAD_LEN)
    {
        return;  
    }

    CWifiParser::Parse(header,pkt_data);
}



int GetPacketFormFile(const char* file) 
{  
    if (NULL == file)
    {
        std::cout<<"The file point is NULL."<<std::endl;
        return -1;
    }

    char errbuf[100];      
    pcap_t *pfile = pcap_open_offline(file, errbuf);  
    if (NULL == pfile) {  
        printf("%s\n", errbuf);  
        return -1;  
    }   
    pcap_pkthdr *pkthdr = 0;  
    const u_char *pktdata = 0;  
    while(pcap_next_ex(pfile, &pkthdr, &pktdata) == 1)
    {
            CWifiParser::Parse(pkthdr,pktdata);
    }
    
    pcap_close(pfile);  
    return 0;  
}  

int main()
{
    int ret = 0;
    int iType = 0;
    while(1)
    {
        std::cout<<"1、Get packet from dev.\n2、Get packet from pcap file.\nPlease enter type number:";
        std::cin>>iType;
        std::cin.get();//去掉cin留在缓冲区的回车符，便于后面的cin.get函数
        if (1 == iType)
        {
            ret = GetPacketFormDev();
        }
        else if(2 == iType)
        {
            char file[MAX_PATH_LEN] = "\0";
            std::cout<<"Please enter the file path:\n[Defaut:cap.pcap]";
            std::cin.get(file,MAX_PATH_LEN);
            std::cin.get();
            if('\0' == file[0])
            {
                strncpy(file,"cap.pcap",MAX_PATH_LEN);
            }
            
            ret =  GetPacketFormFile(file);
        }
        else
        {
            std::cout<<"ERROR: wrong cmd!"<<std::endl;
        }
        std::cin.clear();
        if( 0 != ret)
        {
            char   buffer[MAX_PATH_LEN];   
            getcwd(buffer, MAX_PATH_LEN);   
            printf( "The   current   directory   is:   %s ",   buffer); 
            sleep(5000);
            return ret;
        }
    }
    return 0;
}