/***********************************************************************
Copyright (C)：
File name    ：util_func.h
CLR          ：10.0
Description  ：一些公共的函数定义
Author       ：Deng Renjie [1951]
Date         ：2016/3/29 17:26:08
Modification ：
***********************************************************************/

#ifndef _UTIL_FUNC_H_
#define _UTIL_FUNC_H_
#include "util.h"
#include <vector>

//清除字符开始和结尾的换行符和空格等
void clear_string(std::string& strData);

//根据关键字符串分隔字符串
void splitByString(const std::string& src, const std::string& separate_character, std::vector<std::string>& strs);

//根据关键字符分隔字符串
void splitByChar(const std::string& src, char separate_character, std::vector<std::string>& strs);

//获取字符串data中begin和end标志之间的内容，并返回最后遍历的位置偏移量。
MY_RESULT GetValue(const std::string& data, 
	const std::string& begin,const std::string& end, std::string& strResult,size_t &endRet);

//获取字符串data中begin和end标志之间的内容
MY_RESULT GetValue(const std::string& data, 
	const std::string& begin,const std::string& end, std::string& strResult);

#endif