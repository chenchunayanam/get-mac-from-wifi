/*
����ģ��
*/
#ifndef _SINGLETON_H_
#define _SINGLETON_H_


#define SINGLETON_DECLARE(T) \
	public:\
		static T& CreateInstance(); \
	private:\
		static T m_instance;

#define SINGLETON_IMPLEMENT(T) \
	T T::m_instance;\
	T& T::CreateInstance()\
	{\
		return m_instance;\
	}

#define SINGLETON(T) \
	T::CreateInstance()

#endif //_SINGLETON_H_