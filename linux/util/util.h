/***********************************************************************
Copyright (C)：
File name    ：util.h
CLR          ：10.0
Description  ：一些公共的定义
Author       ：Deng Renjie [1951]
Date         ：2016/3/29 17:25:51
Modification ：
***********************************************************************/

#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>
#include <stdint.h>
#include <pcap.h>
#include "my_log.h"
#include <sstream>

#include <string>
#include <map>
//类型统一定义
#define MY_INT int
#define MY_BOOL bool
#define MY_TRUE true
#define MY_FALSE false
#define MY_LONG long
#define MY_SHORT short
#define MY_UINT unsigned int
#define MY_ULONG unsigned long
#define MY_STR char*
#define MY_CHAR char
#define MY_CSTR const char*
#define MY_VOID void

//不常用类型定义
#define MY_UINT16	uint16_t
#define MY_UINT32	uint32_t
#define MY_UINT64	uint64_t
#define MY_USHORT	u_short
#define MY_UCHAR	u_char
typedef std::map<std::string,std::string> STRING_MAP;

//文件相关通用定义
#define MAX_PATH_LEN 128


#define MY_RESULT int
#define MY_SUCCESS 1
#define MY_FAILED  0

//-----------[start]无线wifi相关参数定义--------------
#define RADIOTAP_HEAD_LEN 8
#define FRAME_HEAD_TYPE_LEN 4	//判断报文类型的数据头长度
#define FRAME_HEAD_ADDRESS_LEN 18	//探查请求和响应mac地址的数据头长度
#define MAX_MAC_LEN 6
#define	MAC_BUFF_LEN 32



#ifdef WIN32
#define SWAP16(x) (x)
#define SWAP32(x) (x)
#pragma pack(1)
struct radiotap_hdr {
	u_int8_t        it_version;     /* set to 0 */
	u_int8_t        it_pad;
	u_int16_t       it_len;         /* entire length */
	u_int32_t       it_present;     /* fields present */
};

struct hdr_type {
	u_int8_t        type;     /* version  type subtype*/
	u_int8_t        flags;
	u_int16_t       duration;
};

struct hdr_address {
	u_char       	desMac[MAX_MAC_LEN];         /* MAC */
	u_char       	tranMac[MAX_MAC_LEN];        /* MAC */
	u_char       	bssMac[MAX_MAC_LEN];         /* MAC */
};
#pragma pack()
#else // WIN32
#define SWAP16(x) ((x&0x00ff) << 8 | (x&0xff00) >> 8)
#define SWAP32(x) ((x&0x000000ff) << 24 | (x&0x0000ff00) << 8 | (x&0x00ff0000) >> 8 | (x&0xff000000) >> 24)
struct radiotap_hdr {
	u_int8_t        it_version;     /* set to 0 */
	u_int8_t        it_pad;
	u_int16_t       it_len;         /* entire length */
	u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));

struct hdr_type {
	u_int8_t        type;     /* version  type subtype*/
	u_int8_t        flags;
	u_int16_t       duration;
} __attribute__((__packed__));

struct hdr_address {
	u_char       	desMac[MAX_MAC_LEN];         /* MAC */
	u_char       	tranMac[MAX_MAC_LEN];        /* MAC */
	u_char       	bssMac[MAX_MAC_LEN];         /* MAC */
} __attribute__((__packed__));
#endif		//WIN32

//-----------[end]无线wifi相关参数定义--------------

//报文头域关键字
#define HEAD_HOST_KEY "Host"
#define HEAD_URL_KEY "URL"

#define ETHER_ADDR_LEN 6 /* ethernet address */
#define ETHER_HEAD_LEN 14 /* ethernet head length */
#define ETHERTYPE_IP 0x0800 /* ip protocol */
#define TCP_PROTOCAL 0x0600 /* tcp protocol */
#define BUFFER_MAX_LENGTH 65536 /* buffer max length */


//安全删除指针
#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if(p){delete(p);  (p)=NULL;} }
#endif

//安全删除指针数组
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) \
{ \
	if(p)\
	{\
		delete[] (p);\
		(p)=NULL;\
	}\
}
#endif

#pragma pack(1)
/*
* define struct of ethernet header , ip address , ip header and tcp header
*/
/* ethernet header */
typedef struct ether_header {
	MY_UCHAR ether_shost[ETHER_ADDR_LEN]; /* source ethernet address, 8 bytes */
	MY_UCHAR ether_dhost[ETHER_ADDR_LEN]; /* destination ethernet addresss, 8 bytes */
	MY_USHORT ether_type;                 /* ethernet type, 16 bytes */
}ether_header;

/* TCP 首部 */
typedef struct header_tcp
{
	MY_USHORT src_port;
	MY_USHORT dst_port;
	MY_UINT32 seq;
	MY_UINT32 ack_seq;
	MY_USHORT doff:4,hlen:4,fin:1,syn:1,rst:1,psh:1,ack:1,urg:1,ece:1,cwr:1;
	MY_USHORT window;
	MY_USHORT check;
	MY_USHORT urg_ptr;
}tcp_header;

/* IPv4 首部 */
typedef struct ip_header
{
	MY_UCHAR ver_ihl; // 版本 (4 bits) + 首部长度 (4 bits)
	MY_UCHAR tos; // 服务类型(Type of service)
	MY_USHORT tlen; // 总长(Total length)
	MY_USHORT identification; // 标识(Identification)
	MY_USHORT flags_fo; // 标志位(Flags) (3 bits) + 段偏移量(Fragment offset) (13 bits)
	MY_UCHAR ttl; // 存活时间(Time to live)
	MY_UCHAR proto; // 协议(Protocol)
	MY_USHORT crc; // 首部校验和(Header checksum)
	MY_UINT32 saddr; // 源地址(Source address)
	MY_UINT32 daddr; // 目的地址(Destination address)
	MY_UINT32 op_pad; // 选项与填充(Option + Padding)
} ip_header;
#pragma pack()

//数据类型转换string -> T
template <class T>
T StringTo(std::string s)
{
	T value;
	std::stringstream(s) >> value;
	return value;
}

//T->String
template <class T>
std::string ToString(T value)
{
	std::stringstream s;
	s << value;
	return s.str();
}

#endif //_UTIL_H_