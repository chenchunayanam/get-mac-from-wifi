/***********************************************************************
Copyright (C)：
File name    ：my_log.h
CLR          ：10.0
Description  ：日志管理类
Author       ：Deng Renjie [1951]
Date         ：2016/3/29 17:25:22
Modification ：
***********************************************************************/

#ifndef _MY_LOG_H_
#define _MY_LOG_H_

#include <cstdio>
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <cstdarg>
#include "singleton.h"

//#define _TRACE_OPEN_	//TRACE日志是否打开
//#define _DEBUG_OPEN_	//DEBUG日志是否打开
#define _ERROR_OPEN_	//ERR日志是否打开
const int OP_SUCCESS = 0;
const int OP_FAILED = -1;
static std::string MOD_NAME = "GET_MAC";//模块名
static const  char logFilePath[] = "log.txt"; //日志文件存放路径
static const  char DataFilePath[] = "data.txt"; //其他数据文件存放路径

#ifdef _DEBUG_OPEN_
#define LOG_DEBUG(cFormat, ...) \
	SINGLETON(CMyLog).LogRecord(__FUNCTION__, __LINE__, MOD_NAME, OP_SUCCESS, cFormat, __VA_ARGS__);
#else 
#define LOG_DEBUG(cFormat, ...) void ();
#endif

#ifdef _ERROR_OPEN_
#define LOG_ERR(cFormat, ...) \
	SINGLETON(CMyLog).LogRecord(__FUNCTION__, __LINE__, MOD_NAME, OP_SUCCESS, cFormat, __VA_ARGS__);
#else 
#define LOG_ERR(cFormat, ...) void ();
#endif

#ifdef _TRACE_OPEN_
#define LOG_TRACE(cFormat, ...) \
	SINGLETON(CMyLog).LogRecord(__FUNCTION__, __LINE__, MOD_NAME, OP_SUCCESS, cFormat, __VA_ARGS__);
#else 
#define LOG_TRACE(cFormat, ...) void ();
#endif

#define LOG_DATA(cFormat, ...) \
	SINGLETON(CMyLog).WriteData(cFormat, __VA_ARGS__);

class CMyLog
{
	SINGLETON_DECLARE(CMyLog);
public:
	/************************************************************************/
	/*funcDesc:将日志写入到文件中
	/************************************************************************/	
	void LogRecord(std::string strFunc, unsigned int uiLine, std::string strMod, int iulErrCode, char* cFormat, ...);
	
	/************************************************************************/
	/*funcDesc:将日志直接打印到屏幕
	/************************************************************************/	
	void LogDebug(std::string strFunc, unsigned int uiLine, std::string strMod, int iulErrCode, char* cFormat, ...);

	/************************************************************************/
	/*funcDesc:将其他数据内容写到文件
	/************************************************************************/	
	void WriteData( char* cFormat, ...);
};

#endif //_MY_LOG_H_