#include "util_func.h"

void splitByString(const std::string& src, const std::string& separate_character, std::vector<std::string>& strs)
{
	int separate_characterLen = separate_character.size();//分割字符串的长度,这样就可以支持如“,,”多字符串的分隔符
	int lastPosition = 0,index = -1;
	while (-1 != (index = src.find(separate_character,lastPosition)))
	{
		strs.push_back(src.substr(lastPosition,index - lastPosition));
		lastPosition = index + separate_characterLen;
	}

	std::string lastString = src.substr(lastPosition);//截取最后一个分隔符后的内容
	if (!lastString.empty())
		strs.push_back(lastString);//如果最后一个分隔符后还有内容就入队
}

void splitByChar(const std::string& src, char separate_character, std::vector<std::string>& strs)
{
	int separate_characterLen = sizeof(char);//分割字符串的长度,这样就可以支持如“,,”多字符串的分隔符
	int lastPosition = 0,index = -1;
	while (-1 != (index = src.find(separate_character,lastPosition)))
	{
		strs.push_back(src.substr(lastPosition,index - lastPosition));
		lastPosition = index + separate_characterLen;
	}

	std::string lastString = src.substr(lastPosition);//截取最后一个分隔符后的内容
	if (!lastString.empty())
		strs.push_back(lastString);//如果最后一个分隔符后还有内容就入队
}

//清除字符开始和结尾的换行符和空格等
void clear_string(std::string& strData)
{
	size_t beginPos = 0;
	size_t endPos = strData.size();
	if(0 == endPos)
	{
		return;
	}

	MY_BOOL flag = false; //用于标识是否结束匹配
	while (beginPos<endPos)
	{
		if (flag)
		{
			break;
		}

		switch(strData[beginPos])
		{
		case ' ':
		case '\r':
		case '\n':
			++beginPos;
			break;
		default:
			flag = true;
			break;
		}
	}

	flag = false; //用于标识是否结束匹配
	while (beginPos>endPos)
	{
		if (flag)
		{
			break;
		}

		switch(strData[endPos])
		{
		case ' ':
		case '\r':
		case '\n':
			--endPos;
			break;
		default:
			flag = true;
			break;
		}
	}

	strData = strData.substr(beginPos,endPos - beginPos);
}

MY_RESULT GetValue(const std::string& data, 
	const std::string& begin,const std::string& end, std::string& strResult,size_t &endRet)
{
	size_t beginPos = data.find(begin);
	if(std::string::npos == beginPos)
	{
		return MY_FAILED;
	}

	size_t flagLen = begin.size();
	size_t endPos = data.find(end,beginPos + flagLen);
	if(std::string::npos == endPos)
	{
		return MY_FAILED;
	}

	strResult = data.substr(beginPos + flagLen,endPos - beginPos - flagLen);
	clear_string(strResult);
	endRet += endPos + end.size();
	return MY_SUCCESS;
}

MY_RESULT GetValue(const std::string& data, 
	const std::string& begin,const std::string& end, std::string& strResult)
{
	size_t beginPos = data.find(begin);
	if(std::string::npos == beginPos)
	{
		return MY_FAILED;
	}

	size_t flagLen = begin.size();
	size_t endPos = data.find(end,beginPos + flagLen);
	if(std::string::npos == endPos)
	{
		return MY_FAILED;
	}

	strResult = data.substr(beginPos + flagLen,endPos - beginPos - flagLen);
	clear_string(strResult);
	return MY_SUCCESS;
}